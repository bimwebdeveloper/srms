<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company_Information extends Model
{
    //
     protected $fillable = [
        'description', 'address','phone','email','web','fax','logo','status'
    ];

}
