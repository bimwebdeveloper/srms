<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company_Information;
use Illuminate\Support\Facades\Session;
class CompanyInformationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages= Company_Information::all();
        return view('admin.companyinformation.index', compact('pages'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.companyinformation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           $requestData=$request->all();
        if($request->hasFile('logo'))
        {
            $filename=rand(1,9999).time().'.'.$request->file('logo')->getClientOriginalExtension();
            $path='uploads/'.$filename;
            $file=$request->file('logo');
            $file->move('uploads',$filename);    
        }else
            $path='NULL';
        $requestData['logo']=$path;
        $page= Company_Information::create($requestData);
       if($page)
       {
           
           return redirect('admin/companyinformation')->with('alert', 'Inserted!');
           
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page= Company_Information::findorfail($id);
        return view('admin.companyinformation.edit',compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData=$request->all();
        $page= Company_Information::findorFail($id);
        
        if($request->hasFile('logo'))
        {
               if(is_file($page->logo)&& file_exists($page->logo))
               unlink($page->logo);
               
            $filename=rand(1,9999).time().'.'.$request->file('logo')->getClientOriginalExtension();
            $path='uploads/'.$filename;
            $file=$request->file('logo');
            $file->move('uploads',$filename);    
        }else
        {$path=$page->logo;}
        
        $requestData['logo']=$path;
        //$page ->update($requestData);
        $page ->update([
            'description'=>$request['description'],
            'address'=>$request['address'],
            'phone'=>$request['phone'],
            'email'=>$request['email'],
            'web'=>$request['web'],
            'fax'=>$request['fax'],
            'logo'=>$path,
            'status'=>$request['status'],
        ]);
        //$page->roll = $request['roll'];
        //$page->save();
          if($page)
       {
           
           return redirect('admin/companyinformation')->with('alert', 'updated!');
           
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page=Company_Information::findOrFail($id);
        if(is_file($page->logo)&& file_exists($page->logo))
        {
            unlink($page->logo);
        }
        $page->delete();
        if($page)
        {
            $test="deleted".$page->name;
        Session::flash('message',$test);
        }
        else
            Session::flash('error_message','Try Again!!');
        return redirect('admin/companyinformation');
    }
}
