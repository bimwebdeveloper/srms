<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Semester;

class SemesterController extends Controller
{
    
    public function index()
    {
       $pages=Semester:: all();
       return view ('admin.semester.semesterview',compact('pages'));
    }
     public function create()
    {
       return view('admin.semester.semesteradd');
    }
 
    public function store(Request $request)
    {
        
        $requestData=$request->all();
        $requestData['status']=1;
        $pages = Semester::create($requestData);
        return redirect('admin/semester');
    }
   
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $pages=Semester::findOrFail($id);
        return view('admin.semester.semesterupdate',compact('pages'));
    }

    
    public function update(Request $request, $id)
    {
        $requestedData=$request->all();
        $pages=Semester::findOrFail($id);  
        $pages->update
        ([
        'year'=>$request['year'],
        'part'=>$request['part'],
        'sem_name'=>$request['sem_name'] ,]);
        if($pages)
        {return redirect('admin\semester');
        
        }
        else
        {    return('error');
        }  
    }

       public function destroy($id)
    {
         $pages=Semester::findOrFail($id); 
       
        $pages->delete();
        return redirect()->action('Admin\SemesterController@index');
    }
}
