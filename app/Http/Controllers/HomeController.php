<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::check()) {
                switch (Auth::user()->types) {
                    case array_search('Admin', config('custom.user_types')):
                        return redirect('admin');
                        break;
                    case array_search('Teacher', config('custom.user_types')):
                        return redirect('teacher');
                        break;
                    case array_search('Counselor', config('custom.user_types')):
                        return redirect('counselor');
                        break;
                    case array_search('HoD', config('custom.user_types')):
                        return redirect('hod');
                        break;
                    default:
                        return redirect('');
                        break;
                }
        }
    }
}
