<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class TeacherMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() && Auth::user()->types == array_search('Teacher', config('custom.user_types')))
         return $next($request);
        else if(Auth::check() && Auth::user()->types == array_search('Admin', config('custom.user_types')))
          return redirect('admin');
        else if(Auth::check() && Auth::user()->types == array_search('HoD', config('custom.user_types')))
                return redirect('hod');
        else if(Auth::check() && Auth::user()->types == array_search('Counselor', config('custom.user_types')))
                return redirect('counselor');
    }
}
