<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company__information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('description');
             $table->String('address');
            $table->String('phone');
            $table->String('email');
            $table->String('web');
            $table->String('fax');
            $table->String('logo');
            $table->Integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company__information');
    }
}
