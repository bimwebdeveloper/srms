@extends('layouts.master')
@section('content')
<section class="content">

     <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
         
            <!-- form start -->
          
          </div>
        
        </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">
       
         
          <!-- /.box -->
          <!-- general form elements disabled -->
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Create Company</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! Form::open(['url'=>'admin/companyinformation','method'=>'post','files'=>true]) !!}
<!--              <form action="{{url('student')}}" method="post">
               <input type="hidden" name="_token" value="<?php // echo csrf_token();?>">
                 text input -->
                <div class="form-group">
                  <label>Description</label>
                  <input type="text" name="description" class="form-control" placeholder="company name">
                </div>
                <div class="form-group">
                <label>Address</label>
                <input type="text" name="address" class="form-control" placeholder="address">
                </div>
               
                 <div class="form-group">
                 <label>phone</label>
                  <input type="text" name="phone" class="form-control" placeholder="phone">
                </div>
                
                <div class="form-group">
                  <label>email</label>
                  <input type="text" name="email" class="form-control" placeholder="email">
                </div>
                
                <div class="form-group">
                  <label>web</label>
                  <input type="text" name="web" class="form-control" placeholder="website">
                </div>
            
                <div class="form-group">
                <label>Fax</label>
                  <input type="text" name="fax" class="form-control" placeholder="fax">
                </div>
                
               <div class="form-group">
                         <label for="logo">logo</label>
                            <input type="file" name='logo' >
                           </div>
{!! Form::label('status', 'Status', ['class' => 'form-group']) !!}
                            <div class="form-group">
                                {!! Form::select('status',['1'=>'Active','0'=>'Inactive'], null, ['class' => 'form-control']) !!}
                                {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                            </div>
                
                  <!-- /input-group -->
                </div>
                <!-- /.col-lg-6 -->
              </div>

               <input type="submit" name="submit">
               
               @if($errors->any())
              
               @foreach($errors->all() as $error)
               {{$error}}
               @endforeach
               @endif
                <!-- Select multiple-->
               
               

              <!--</form>-->
              {!!Form::close()!!}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
@endsection


<!--

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        
        <h1 style="background-color:DodgerBlue;">FILL IN STUDENT DETAILS</h1>
        <form action="{{url('student')}}" method="post">
            <input type="hidden" name="_token" value="<?php echo csrf_token();?>">
            Name:<input type ="text" name="name"><br>
            Roll:  <input type ="integer" name="roll"><br>
            Fee:<input type ="integer" name="fee"><br>
            Address:  <input type ="text" name="address"><br>
            <input type="submit" name="submit">
        </form>   
    </body>
</html>
--!>
