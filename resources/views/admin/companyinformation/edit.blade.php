@extends('layouts.master')
@section('content')
<section class="content">

     <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
         
            <!-- form start -->
          
          </div>
        
        </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">
       
         
          <!-- /.box -->
          <!-- general form elements disabled -->
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Company Information</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! Form::model($page,['method'=>'patch','url'=>['admin/companyinformation',$page->id],'files'=>true]) !!}
<!--             
                 text input -->
                <div class="form-group">
                  <label>Description</label>
                  {!!Form::text('description',null,['class'=>'form-control','id'=>'Description'])!!}
                </div>
                <div class="form-group">
                <label>Address</label>
                {!!Form::text('address',null,['class'=>'form-control','id'=>'Address'])!!}
                </div>
               
                 <div class="form-group">
                     <label>phone</label>
                  {!!Form::text('phone',null,['class'=>'form-control','id'=>'Phone'])!!}
                 </div>
                
                <div class="form-group">
                  <label>Email</label>
                  {!!Form::text('email',null,['class'=>'form-control','id'=>'Email'])!!}
                </div>
                
                <div class="form-group">
                  <label>Web</label>
                  {!!Form::text('web',null,['class'=>'form-control','id'=>'Web'])!!}
                </div>
            
                <div class="form-group">
                <label>Fax</label>
                  {!!Form::text('fax',null,['class'=>'form-control','id'=>'Fax'])!!}
                </div>
                
               <div class="form-group">
                         <label for="Logo">logo</label>
                            {!!Form::file('logo',null,['class'=>'form-control','id'=>'Logo'])!!}
                            <img src="{{url($page->logo)}}" height="100px" width="100px"/>
                           </div>
                            {!! Form::label('status', 'Status', ['class' => 'form-group']) !!}
                            <div class="form-group">
                                {!! Form::select('status',['1'=>'Active','0'=>'Inactive'], null, ['class' => 'form-control']) !!}
                                {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                            </div>
                
                  <!-- /input-group -->
                </div>
                <!-- /.col-lg-6 -->
              </div>

               <input type="submit" name="submit">
               
               @if($errors->any())
              
               @foreach($errors->all() as $error)
               {{$error}}
               @endforeach
               @endif
                <!-- Select multiple-->
               
               

              <!--</form>-->
              {!!Form::close()!!}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
@endsection
