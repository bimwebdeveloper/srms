@extends('layouts.master')
@section('content')

 <section class="content">
     
     
         
      <!-- /.row -->
      <a href="{{url('admin/companyinformation/create')}}"> <button class="btn btn-bitbucket">ADD</button></a>
      @if(Session::has('message'))
      <p class='alert alert-info' >{{Session::get('message')}}</p>
      @endif
      @if(Session::has('error_message'))
      {{Session::get('error_message')}}
      @endif
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Company Information</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                 <th>Description</th>
                    <th>Address</th>
                    <th>Phone No</th>
                    <th>Email</th>
                    <th>Web</th>
                    <th>Fax</th>
                    <th>Logo</th>
                    <th>Status</th>
                </tr>
                
                 <?php $s=1;?>
                @foreach($pages as $page)
                
                <tr>
                    
                   
                    <td>{{$page->description}}</td>
                    <td>{{$page->address}}</td>
                    <td>{{$page->phone}}</td>
                    <td>{{$page->email}}</td>
                    <td>{{$page->web}}</td>
                    <td>{{$page->fax}}</td>
                   <td><img src="{{url($page->logo)}}" width="100" height="100" /></td> 
                   <td>{{$page->status}}</td>
                   <td><a href="{{url('admin/companyinformation/'.$page->id.'/edit')}}"> <button type="button" <small class="label pull-right bg-blue">Edit</small></button></td>
                   <td>{!! Form::model($page,['method'=>'delete','url'=>['admin/companyinformation',$page->id]])!!}  <button type="submit" onclick="return confirm('Are you sure?')"<small class="label pull-right bg-red-active">Delete</small></button></td>
                  {!!Form::close()!!}
                </tr>
                
                @endforeach
                
              
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
@endsection


<!--
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Students List Here</title>
    
        
    <body>
         <h1> students list here </h1>
         <a href="{{url('student/create')}}"><button> create</button></a>
         <button>ADD</button>
         
        <table border="1">
            
            <thead>
                <tr>
                    <th>S.no</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>roll</th>
                    <th>fee</th>
                    <th>address</th>
                </tr>
            </thead>
            <tbody>
                      <script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if(exist){
      alert(msg);
    }
  </script>
                <?php $s=1;?>
                @foreach($pages as $page)
                
                <tr>
                    
                    <td>{{$s++}}</td>
                    <td>{{$page->id}}</td>
                    <td>{{$page->name}}</td>
                    <td>{{$page->roll}}</td>
                    <td>{{$page->fee}}</td>
                    <td>{{$page->address}}</td>
                    <td> <a href="{{url('student/'.$page->id.'/edit')}}"><button> edit</button></a></td>
                    <td>
                         {!! Form::model($page,[
        'method' =>'DELETE',
        'url' =>['/student',$page->id],
        ]) !!}
        <button type='submit'> delete</button>
                     {!!Form::close() !!}
                
                </tr>
               
                    @endforeach
                
                
                
            </tbody>
        
        </body>
        
</html>
-->>