
@extends('layouts.master')
@section('content')
<section class="content">
      <div class="row">
        <!-- left column -->
    
        </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Semester Creation Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!!Form::open(['url'=>'admin/semester','method'=>'POST', 'class'=>'form-horizontal','files'=>true])!!}
            <!--<form action="{{url('Student')}}" method="POST" class="form-horizontal">-->
            <input type="hidden" name="_token" value="<?php echo csrf_token();?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Year</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" name="year" placeholder="Year">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Part</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputPassword3" name="part" placeholder="Part">
                  </div>
                </div>
                <div class="form-group">
                  <label for="photo" class="col-sm-2 control-label">Semester</label>

                  <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputPassword3" name="sem_name" placeholder="Semester">
                  </div>
                </div>
               
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-default">Submit</button>
           
              </div>
              <!-- /.box-footer
            </form> -->
            {{ Form::close() }}
          </div>
          <!-- /.box -->
          <!-- general form elements disabled -->
 
                </div>

              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>


    @endsection

<!--

<form action="{{url('Student')}}" method="POST">
    <input type="hidden" name="_token" value="<?php echo csrf_token();?>">
Name:<input type="text" name="name"><br><br>
Roll:<input type="text" name="roll"><br><br>
<input type="submit" name="submit">-->


