
@extends('layouts.master')
@section('content')
<section class="content">
      <div class="row">
        <!-- left column -->
    
        </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Semester Edit Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
            <!--<form action="{{url('Student')}}" method="POST" class="form-horizontal">-->
            {!! Form::model($pages,
            ['method' => 'PATCH',
            'url' => ['admin/semester', $pages->id], 
            'class'=>'col-sm-2 control-label',
            'files'=>true])!!}

            Year:{!!Form::text('year')!!}
            Part:{!!Form::text('part')!!}
            Semester:{!!Form::text('sem_name')!!}
            <input type="submit">
            
              {{ Form::close() }}
          </div>
          <!-- /.box -->
          <!-- general form elements disabled -->
 
                </div>

              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>


    @endsection

