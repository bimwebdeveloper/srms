
@extends('layouts.master')
@section('content')
<section class="content">
<a href="{{url('admin/semester/create')}}"><button type="submit" class="btn btn-danger">
                      Add
                    </button></a>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Semester List</h3>
              
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                   
                  </div>
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search" ></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>ID</th>
                  <th>Semester</th>
                  <th>Year</th>
                  <th>Part</th>
                  <th>Edit</th>
                  <th>Delete</th>
                  
                </tr>
                <?php $i=1;?>
                @foreach($pages as $pages)

                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$pages->sem_name}}</td>
                  <td>{{$pages->year}}</td>
                  <td>{{$pages->part}}</td>
                  
                  <td><a href="{{url('admin/semester/'.$pages->id. '/edit')}}"><button>Edit</button> </a></td>          
                  <td>{!! Form::model($pages,['method' => 'DELETE','url' => ['admin/semester', $pages->id]])!!}
                    <button type="submit">Delete</button>
		              {!!Form::close()!!}
               </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    @endsection