<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => 'auth'], function () {
    
    Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function () {
        Route::resource('', 'Admin\HomeController');
        Route::resource('semester', 'Admin\SemesterController');
		 Route::resource('companyinformation', 'Admin\CompanyInformationsController');
        
        
        
    });
    Route::group(['middleware' => 'teacher', 'prefix' => 'teacher'], function () {
        Route::resource('', 'Teeacher\HomeController');
        
    });
    Route::group(['middleware' => 'counselor', 'prefix' => 'counselor'], function () {
        Route::resource('', 'Counselor\HomeController');
        
    });
    Route::group(['middleware' => 'hod', 'prefix' => 'hod'], function () {
        Route::resource('', 'Hod\HomeController');
        
    });
    
});
